from django.db import models

# Create your models here.

class URL(models.Model):
    original_url = models.URLField(max_length=200)
    short_url = models.CharField(max_length=64, unique=True)
    full_short_url = models.URLField(max_length=200)
