from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import URL

def get_counter():
    counter = 1
    ok = False
    if URL.objects.count() != 0:
        while not ok:
            search_url = URL.objects.filter(short_url=str(counter))
            if search_url:
                counter += 1
            else:
                ok = True
    return counter


def shorten_url(request):

    shortened_urls = URL.objects.all()

    if request.method == "POST":
        original_url = request.POST["original_url"]
        short_url = request.POST["short_url"]

        if not original_url:
            return HttpResponse("Please provide a URL to shorten.")

        if not short_url:
            counter = get_counter()
            short_url = str(counter)

        if URL.objects.filter(short_url=short_url).exists():
            return render(request, "acorta/index.html", {"error_message": "Short URL already exists. Please choose another.", "shortened_urls": shortened_urls})

        # Formar la URL con el host y el puerto dinamicamente
        host = request.get_host()
        port = request.META.get("SERVER_PORT", 80)
        full_short_url = f"{host}/{short_url}"

        new_url = URL(original_url=original_url, short_url=short_url, full_short_url=full_short_url)
        new_url.save()

    return render(request, "acorta/index.html", {"shortened_urls": shortened_urls})


def redirect_original_url(request, short_url):
    url_object = get_object_or_404(URL, short_url=short_url)
    original_url = url_object.original_url
    return redirect(original_url)
